//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //in this function change levelName
    override func viewDidLoad() {
        levelName = "L55H" // level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        pyramyd(7)
        
    }
    
    
    
    func pyramyd(_ step:Int){
        var i = 0 // счетчик
        for _ in 0..<step {
            moveX(i)
            i+=1
        }
    }
    
    // функция делает Х шагов вперед, ставя конфетки, разворачивается и идёт назад
    // и переходит на следующую строку
    func moveX(_ count:Int){
        for _ in 0..<count {
            put()
            move()
        }
        turnAround()
        while frontIsClear {
            move()
        }
        turnLeft()
        if frontIsClear {
            move()
        }
        turnLeft()
    }
    
    
    func turnAround() {
        turnRight()
        turnRight()
    }
    func turnLeft (){
        turnRight()
        turnRight()
        turnRight()
    }
    
}


